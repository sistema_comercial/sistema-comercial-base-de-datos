﻿
IF  EXISTS (SELECT * FROM sys.objects WHERE name = 'dividirCadena' AND type = 'IF')
	DROP FUNCTION dividirCadena
GO

CREATE FUNCTION [dbo].[dividirCadena]
(
    @sString nvarchar(2048),
    @cDelimiter nchar(1)
)
RETURNS @tParts TABLE ( part nvarchar(2048) )
AS
BEGIN
    if @sString is null return
    declare @iStart int,
            @iPos int
    if substring( @sString, 1, 1 ) = @cDelimiter 
    begin
        set @iStart = 2
        insert into @tParts
        values( null )
    end
    else 
        set @iStart = 1
    while 1=1
    begin
        set @iPos = charindex( @cDelimiter, @sString, @iStart )
        if @iPos = 0
            set @iPos = len( @sString )+1
        if @iPos - @iStart > 0          
            insert into @tParts
            values  ( substring( @sString, @iStart, @iPos-@iStart ))
        else
            insert into @tParts
            values( null )
        set @iStart = @iPos+1
        if @iStart > len( @sString ) 
            break
    end
    RETURN

END
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE name = 'fu_buscar_descripcion_concepto' AND type = 'FN')
	DROP FUNCTION fu_buscar_descripcion_concepto
GO
----------------------------------------------------------------------------------
-- Author: Jonathan Macalupu S. IDE-SOLUTION
-- Created: 20/01/2020  
-- Sistema: Sistema Distribuidora Virgen del Carmen
-- Modulo: General
-- Descripción: Funcion que trae descripciones 
-----------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[fu_buscar_descripcion_concepto] 
(   @p_flag SMALLINT,   --Flag de proceso
	@p_codigo INT,      --Codigo de Concepto
	@p_correlativo INT  --Correlativo de Concepto
) RETURNS VARCHAR(250)
AS
BEGIN
   DECLARE @Valor_Retorno AS VARCHAR(250)  --Valor de la descripcion

   IF @p_flag = 1
	   SELECT @Valor_Retorno = descripcion 
	   FROM tblConcepto 
	   WHERE codConcepto = @p_codigo AND correlativo = @p_correlativo AND marcaBaja = 0;
   IF @p_flag = 2
	   SELECT @Valor_Retorno = descripcion
	   FROM tblRutas
	   WHERE ntraRutas = @p_codigo AND marcaBaja = 0;

   RETURN @Valor_Retorno
END
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE name = 'fu_buscar_conceptos_generales' AND type = 'IF')
	DROP FUNCTION fu_buscar_conceptos_generales
GO

----------------------------------------------------------------------------------
-- Author: Luis Llatas IDE-SOLUTION
-- Created: 10/02/2020  
-- Sistema: WEB
-- Modulo: General
-- Descripción: buscar conceptos generales (todos) 
-----------------------------------------------------------------------------------

CREATE FUNCTION fu_buscar_conceptos_generales
 (@p_codigo INT)
 RETURNS TABLE
 AS
 RETURN (
 SELECT correlativo, descripcion FROM tblConcepto 
 WHERE codConcepto = @p_codigo AND marcaBaja = 0 AND correlativo > 0
 )
GO

